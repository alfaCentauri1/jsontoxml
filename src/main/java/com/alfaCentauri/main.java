package com.alfaCentauri;

import com.github.underscore.U;

public class main {
    public static void main(String[] args) {
        String xml = "<template_element SEGMENT=\"1\"><template_sub_element attr2=\"val2\">Algun valor</template_sub_element></template_element>";
        System.out.println(U.xmlToJson(xml));
        System.out.println(U.jsonToXml(U.xmlToJson(xml)));
        //
        System.out.println("Prueba con xml ");
        xml = "<template_element SEGMENT =\"1\">\n" +
                "                <propertie>ARAB001</propertie>\n" +
                "                <propertie2>720</propertie2>\n" +
                "                <propertie3>EA</propertie3>\n" +
                "                <propertie4>ARAB</propertie4>\n" +
                "                <propertie5>MA01</propertie5>\n" +
                "                <propertie6>20260715</propertie6>\n" +
                "                <propertie7>000000000000000099</propertie7>\n" +
                "            </template_element>";
        String json = U.xmlToJson(xml);
        System.out.println("Resultado JSON ");
        System.out.println(json);
        System.out.println("Salida a XML");
        System.out.println(U.jsonToXml(json));
    }
}
